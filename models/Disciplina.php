<?php

namespace app\models;

use Yii;

class Disciplina extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'disciplina';
    }

    public function rules()
    {
        return [
            [['credito', 'user_create', 'user_update'], 'integer'],
            [['data_create', 'data_update'], 'safe'],
            [['nome', 'cod_cred', 'semestre', 'status'], 'string', 'max' => 45],
            [['user_create'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_create' => 'id']],
            [['user_update'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_update' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'cod_cred' => 'Cod Cred',
            'credito' => 'Credito',
            'semestre' => 'Semestre',
            'status' => 'Status',
            'data_create' => 'Data Create',
            'user_create' => 'User Create',
            'data_update' => 'Data Update',
            'user_update' => 'User Update',
        ];
    }

    public function getUserCreate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_create']);
    }

    public function getUserUpdate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_update']);
    }

    public function getDisciplinaAlunoTurmas()
    {
        return $this->hasMany(DisciplinaAlunoTurma::className(), ['id_disciplina' => 'id']);
    }

    public function getDisciplinaPreRequisitos()
    {
        return $this->hasMany(DisciplinaPreRequisito::className(), ['disciplina_id' => 'id']);
    }

    public function getDisciplinaPreRequisitos0()
    {
        return $this->hasMany(DisciplinaPreRequisito::className(), ['pre_disciplina_id' => 'id']);
    }

    public function getDisciplinaTurmas()
    {
        return $this->hasMany(DisciplinaTurma::className(), ['disciplina_id' => 'id']);
    }
}
