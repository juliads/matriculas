<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserHistorico;

class UserHistoricoSearch extends UserHistorico
{

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['matricula', 'codcred', 'status', 'turma'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = UserHistorico::find();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'matricula', $this->matricula])
            ->andFilterWhere(['like', 'codcred', $this->codcred])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'turma', $this->turma]);

        return $dataProvider;
    }
}
