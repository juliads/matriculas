<?php

namespace app\models;

use Yii;


class DisciplinaPreRequisito extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return 'disciplina_pre_requisito';
    }

    public function rules()
    {
        return [
            [['disciplina_id', 'pre_disciplina_id'], 'integer'],
            [['disciplina_id'], 'exist', 'skipOnError' => true, 'targetClass' => Disciplina::className(), 'targetAttribute' => ['disciplina_id' => 'id']],
            [['pre_disciplina_id'], 'exist', 'skipOnError' => true, 'targetClass' => Disciplina::className(), 'targetAttribute' => ['pre_disciplina_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'disciplina_id' => 'Disciplina ID',
            'pre_disciplina_id' => 'Pre Disciplina ID',
        ];
    }

    public function getDisciplina()
    {
        return $this->hasOne(Disciplina::className(), ['id' => 'disciplina_id']);
    }

    public function getPreDisciplina()
    {
        return $this->hasOne(Disciplina::className(), ['id' => 'pre_disciplina_id']);
    }
}
