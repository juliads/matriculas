<?php
namespace app\models;
use Yii;

class UserHistorico extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_historico';
    }
    
    public function rules()
    {
        return [
            [['matricula', 'codcred', 'status', 'turma'], 'string', 'max' => 45],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'matricula' => 'Matricula',
            'codcred' => 'Codcred',
            'status' => 'Status',
            'turma' => 'Turma',
        ];
    }
}
