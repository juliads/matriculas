<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Disciplina;

class DisciplinaSearch extends Disciplina
{

    public function rules()
    {
        return [
            [['id', 'credito', 'user_create', 'user_update'], 'integer'],
            [['nome', 'cod_cred', 'semestre', 'status', 'data_create', 'data_update'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Disciplina::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'credito' => $this->credito,
            'data_create' => $this->data_create,
            'user_create' => $this->user_create,
            'data_update' => $this->data_update,
            'user_update' => $this->user_update,
        ]);

        $query->andFilterWhere(['like', 'nome', $this->nome])
            ->andFilterWhere(['like', 'cod_cred', $this->cod_cred])
            ->andFilterWhere(['like', 'semestre', $this->semestre])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
