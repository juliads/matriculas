<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DisciplinaPreRequisito;

class DisciplinaPreRequisitoSearch extends DisciplinaPreRequisito
{
    
    public function rules()
    {
        return [
            [['id', 'disciplina_id', 'pre_disciplina_id'], 'integer'],
        ];
    }

   
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = DisciplinaPreRequisito::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'disciplina_id' => $this->disciplina_id,
            'pre_disciplina_id' => $this->pre_disciplina_id,
        ]);

        return $dataProvider;
    }
}
