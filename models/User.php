<?php
namespace app\models;
use Yii;
use \yii\db\ActiveRecord;
use \yii\web\IdentityInterface;
use \yii\base\NotSupportedException;
use \yii\behaviors\TimestampBehavior;

class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE  = 10;
    public $arquivo;
    public static function tableName()
    {
        return 'user';
    }
  
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
  
    public function rules()
    {
        return [
            [['matricula', 'status', 'user_create'], 'integer'],
            [['data_create', 'data_update', 'updated_at', 'created_at'], 'safe'],
            [['authKey'], 'string'],
            [['username', 'nome'], 'string', 'max' => 45],
            [['email', 'passwordHash', 'passwordResetToken'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['username'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['user_create'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_create' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'passwordHash' => 'Password Hash',
            'passwordResetToken' => 'Password Reset Token',
            'matricula' => 'Matricula',
            'nome' => 'Nome',
            'cpf' => 'Cpf',
            'celular' => 'Celular',
            'status' => 'Status',
            'data_create' => 'Data Criação',
            'user_create' => 'User Create',
            'data_update' => 'Data Atualização',
            'authKey' => 'Auth Key',
            'foto' => 'Foto',
        ];
    }
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        return $scenarios;
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
   
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
   
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }
  
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }
  
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'passwordResetToken' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }
  
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }
   
    public function getId()
    {
        return $this->id;
    }
    
    public function getAuthKey()
    {
        return $this->authKey;
    }
    
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
  
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->passwordHash);
    }
    
    public function setPassword($password)
    {
        $this->passwordHash = Yii::$app->security->generatePasswordHash($password);
    }
   
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->security->generateRandomString();
    }
   
    public function generatePasswordResetToken()
    {
        $this->passwordResetToken = Yii::$app->security->generateRandomString() . '_' . time();
    }
   
    public function removePasswordResetToken()
    {
        $this->passwordResetToken = null;
    }
    public function requestPasswordResetToken($id)
    {
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);
        if (!$user) {
            return false;
        }
        if (!User::isPasswordResetTokenValid($user->passwordResetToken)) {
            $user->generatePasswordResetToken();
        }
        if (!$user->save()) {
            return false;
        }
        return $user->passwordResetToken;
    }
   
    public function getCompromissos()
    {
        return $this->hasMany(Compromisso::className(), ['user_id' => 'id']);
    }

    public function getOportunidades()
    {
        return $this->hasMany(Oportunidade::className(), ['user_id' => 'id']);
    }

    public function getCarteira()
    {
        return $this->hasOne(Carteira::className(), ['id' => 'carteira_id']);
    }

    public function getUserCreate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_create']);
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['user_create' => 'id']);
    }
}