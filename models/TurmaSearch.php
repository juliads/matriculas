<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Turma;

class TurmaSearch extends Turma
{

    public function rules()
    {
        return [
            [['id', 'numero_turma', 'user_create', 'user_update'], 'integer'],
            [['data_create', 'data_update'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Turma::find();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'numero_turma' => $this->numero_turma,
            'data_create' => $this->data_create,
            'user_create' => $this->user_create,
            'data_update' => $this->data_update,
            'user_update' => $this->user_update,
        ]);

        return $dataProvider;
    }
}
