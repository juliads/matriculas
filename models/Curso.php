<?php
namespace app\models;
use Yii;

class Curso extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'curso';
    }
    public function rules()
    {
        return [
            [['nome'], 'string'],
            [['user_create', 'user_update'], 'integer'],
            [['data_create', 'data_update'], 'safe'],
            [['user_create'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_create' => 'id']],
            [['user_update'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_update' => 'id']],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'user_create' => 'User Create',
            'data_create' => 'Data Create',
            'user_update' => 'User Update',
            'data_update' => 'Data Update',
        ];
    }
    public function getUserCreate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_create']);
    }

    public function getUserUpdate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_update']);
    }
}
