<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DisciplinaAlunoTurma;

class DisciplinaAlunoTurmaSearch extends DisciplinaAlunoTurma
{

    public function rules()
    {
        return [
            [['id', 'id_disciplina', 'id_user', 'id_turma', 'user_create', 'user_update'], 'integer'],
            [['situacao', 'data_create', 'data_update'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }


    public function search($params)
    {
        $query = DisciplinaAlunoTurma::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_disciplina' => $this->id_disciplina,
            'id_user' => $this->id_user,
            'id_turma' => $this->id_turma,
            'data_create' => $this->data_create,
            'user_create' => $this->user_create,
            'data_update' => $this->data_update,
            'user_update' => $this->user_update,
        ]);

        $query->andFilterWhere(['like', 'situacao', $this->situacao]);

        return $dataProvider;
    }
    public function search_mat($params)
    {
        $query = DisciplinaAlunoTurma::find()->where(['situacao'=>'MAT']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_disciplina' => $this->id_disciplina,
            'id_user' => $this->id_user,
            'id_turma' => $this->id_turma,
            'data_create' => $this->data_create,
            'user_create' => $this->user_create,
            'data_update' => $this->data_update,
            'user_update' => $this->user_update,
        ]);

        $query->andFilterWhere(['like', 'situacao', $this->situacao]);

        return $dataProvider;
    }
    
}
