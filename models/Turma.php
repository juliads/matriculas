<?php

namespace app\models;

use Yii;

class Turma extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'turma';
    }

    public function rules()
    {
        return [
            [['numero_turma', 'user_create', 'user_update'], 'integer'],
            [['data_create', 'data_update'], 'safe'],
            [['user_create'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_create' => 'id']],
            [['user_update'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_update' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero_turma' => 'Numero Turma',
            'data_create' => 'Data Create',
            'user_create' => 'User Create',
            'data_update' => 'Data Update',
            'user_update' => 'User Update',
        ];
    }

    public function getDisciplinaAlunoTurmas()
    {
        return $this->hasMany(DisciplinaAlunoTurma::className(), ['id_turma' => 'id']);
    }

    public function getDisciplinaTurmas()
    {
        return $this->hasMany(DisciplinaTurma::className(), ['turma_id' => 'id']);
    }

    public function getUserCreate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_create']);
    }

    public function getUserUpdate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_update']);
    }
}
