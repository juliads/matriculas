<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Curso;

class CursoSearch extends Curso
{
    public function rules()
    {
        return [
            [['id', 'user_create', 'user_update'], 'integer'],
            [['nome', 'data_create', 'data_update'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }
    public function search($params)
    {
        $query = Curso::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_create' => $this->user_create,
            'data_create' => $this->data_create,
            'user_update' => $this->user_update,
            'data_update' => $this->data_update,
        ]);

        $query->andFilterWhere(['like', 'nome', $this->nome]);

        return $dataProvider;
    }
}
