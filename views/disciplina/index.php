<?php

use yii\helpers\Html;
use app\models\Helper;
use app\models\Disciplina;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\models\DisciplinaAlunoTurma;


$this->title = 'Lead Lista';
?>

<div class="content-wrapper">
  
    <section class="content-header">
        <h1>
            Disciplinas - Sistemas de Informação - 2018/2       
        </h1>
        <ol class="breadcrumb">
            
        </ol>
    </section>
<img src="http://www.cincork.com/files/dbout/29c32238145de44cbff946806c8aa550.full.jpg?w=305&h=118" style="width: 20%;height: auto;"></div>
    
    <section class="content">
        <section class="content">
            <div class="box">
                    <div class="form-group">
                       
                    </div>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <div class="column col-sm-12">
                        <?php
                            // View file rendering the widget
                            //http://demos.krajee.com/grid-demo
                            echo GridView::widget([
                                'dataProvider' => $dataProvider,                               
                                'rowOptions'=>function($model){
                                    $user = \Yii::$app->user->identity;
                                    $historico = DisciplinaAlunoTurma::find()->where(['id_user'=>$user->id,'id_disciplina'=>$model->id])->orderby(['id'=>SORT_DESC])->one();
                                    if($historico){
                                        if($historico->situacao=='APR')
                                        {    
                                            return ['class' => 'success'];
                                        }
                                        if($historico->situacao=='MAT')
                                        {  
                                            return ['class' => 'warning'];
                                        }
                                    }
                                },
                                        
                                        
                                'pjax' => false, 
                                'bordered' => false,
                                'striped' => false,
                                'condensed' => false,
                                'responsive' => true,
                                'hover' => true,
                                'persistResize' => true,
                                'columns' => [
                                    
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'header'=>'Matricular/Desmatricular',
                                        'headerOptions' => ['class'=>'cor-azul','width' => '80'],
                                        'template' => '{editar}{delete}',
                                        'buttons' => [
                                            'editar' => function ($url,$model) 
                                            {
                                                $user = \Yii::$app->user->identity;
                                                $historico = DisciplinaAlunoTurma::find()->where(['id_user'=>$user->id,'id_disciplina'=>$model->id])->orderby(['id'=>SORT_DESC])->one();
                                                $matriculados = count(DisciplinaAlunoTurma::find()->where(['id_disciplina'=>$model->id,'id_turma'=>'168','situacao'=>'MAT'])->orderby(['id'=>SORT_DESC])->all());
                                                $vagas = 3-$matriculados;
                                                
                                                if(!$historico || $historico->situacao == 'DEL'){
                                                    return Html::a("<i class='glyphicon glyphicon-pencil' title='Vagas Disponíveis: {$vagas}' data-toggle='tooltip'></i>", ['disciplina-aluno-turma/create','id_disciplina'=>$model->id,'id_turma'=>'168','situacao'=>'MAT']).' ';
                                                }
                                                
                                            },
                                            'delete' => function ($url,$model) 
                                            {
                                                $user = \Yii::$app->user->identity;
                                                $matricula = DisciplinaAlunoTurma::find()->where(['id_user'=>$user->id,'id_disciplina'=>$model->id,'id_turma'=>'168','situacao'=>'MAT'])->orderby(['id'=>SORT_DESC])->one();
                                                
                                                if($matricula){
                                                    return Html::a("<i class='glyphicon glyphicon-remove' title='Deseja excluir a matricula?' data-toggle='tooltip'></i>", ['disciplina-aluno-turma/update','id'=>$matricula->id],['method'=>'post']).' ';
                                                }
                                                
                                                    },

                                                  ],
                                            ]  ,
                                    
                                    
                                    [
                                        'label'=>'Disciplinas',
                                        'attribute'=>'id',
                                        'vAlign'=>'middle',
                                        'format'=>'raw',
                                        'value'=>function ($model) {
                                            return $model->nome;
                                        },
                                       
                                    ],
                                    'semestre'

                                ],
                            ]);
                        ?>

                    </div>
                </div>
            </div>
        </section>
    </section>
</div>

    
r
