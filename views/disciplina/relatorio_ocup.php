<?php

use yii\helpers\Html;
use app\models\Helper;
use app\models\DisciplinaTurma;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\models\DisciplinaAlunoTurma;


?>
<div class="content-wrapper">
    <section class="content-header">
    
        <ol class="breadcrumb">
        </ol>
    </section>

    <section class="content">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <div class="form-group">
                        
                    </div>
                    <div class="box-tools pull-right">    
                    </div>
                </div>
                <div class="box-body">
                    <div class="column col-sm-12">
                        
                        <?php
                            $resultado = 0;
                            foreach($arr_disci as $key => $value): ?>
                        <div class="col-md-12">
                            <?php
                                $disciplinaTurma = DisciplinaTurma::find()->where(['disciplina_id'=>$key])->one();
                               
                                echo "<b>Disciplina:</b> {$disciplinaTurma->disciplina->nome}<Br/>";
                            ?>

                            <?php                            
                                $disciplinaTurma = DisciplinaTurma::find()->where(['disciplina_id'=>$key])->one();
                                $resultado = number_format((($value/$disciplinaTurma->vagas)*100),2,",",".");
                                $resultado = $resultado." %";
                                echo "<b>Percentual por disciplina:</b> {$resultado}";
                                echo "<Br/>";
                                echo "   <Br>"
                                
                            ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </section>
    </section>
</div>

    

