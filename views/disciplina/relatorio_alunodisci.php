<?php
use yii\helpers\Html;
use app\models\Helper;
use app\models\Disciplina;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\models\DisciplinaAlunoTurma;

$this->title = 'Lead Lista';
?>
<div class="content-wrapper">
    <section class="content-header">
       
        <ol class="breadcrumb">
        </ol>
    </section>
    <section class="content">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <div class="form-group">
                        
                    </div>
                    <div class="box-tools pull-right">  
                    </div>
                </div>
                <div class="box-body">
                    <div class="column col-sm-12">
                        <?php
                           
                            echo GridView::widget([
                                'dataProvider' => $dataProvider,                           
                                'pjax' => false, 
                                'bordered' => false,
                                'striped' => false,
                                'condensed' => false,
                                'responsive' => true,
                                'hover' => true,
                                'persistResize' => true,
                                'columns' => [
                                    [
                                          'label'=>'Alunos',
                                        'attribute'=>'user.id',
                                        'vAlign'=>'middle',
                                        'format'=>'raw',
                                        'value'=>function ($model) {
                                            return $model->user->id;
                                        },                                       
                                    ],                                               
                                    
                                    [
                                        'label'=>'Turmas',
                                        'attribute'=>'turma.id',
                                        'vAlign'=>'middle',
                                        'format'=>'raw',
                                        'value'=>function ($model) {
                                            return $model->turma->id;
                                        },                               
                                    ], 
                                                
                                    [   
                                        'vAlign'=>'middle',
                                        
                                        'value'=>function ($model) {
                                            return $model->disciplina->nome;
                                        },                                                                     
                                    ],           
                                ],
                            ]);
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </section>
</div>

    

