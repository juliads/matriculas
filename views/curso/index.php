<?php

use yii\helpers\Html;
use yii\grid\GridView;


$this->title = 'Cursos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="curso-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  ?>

    <p>
        <?= Html::a('Create Curso', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nome:ntext',
            'user_create',
            'data_create',
            'user_update',
            //'data_update',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
