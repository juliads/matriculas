<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\DisciplinaAlunoTurma */

$this->title = 'Create Disciplina Aluno Turma';
$this->params['breadcrumbs'][] = ['label' => 'Disciplina Aluno Turmas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disciplina-aluno-turma-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
