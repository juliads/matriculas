<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\detail\DetailView as KartikDetailView;
use app\models\Helper;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */

$this->title = 'Usuário Visualização';

?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Usuário
            <small>Usuário Visualização</small>
        </h1>
        <ol class="breadcrumb">
            <!--<li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Início</a></li>-->
        </ol>
    </section>

    <section class="content">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <div class="form-group">
                        <?= Html::a(Yii::t('app', 'Atualizar Dados'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?=
                        Html::a(Yii::t('app', 'Excluir Conta'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?')
                            ],
                        ])
                        ?>
                    </div>
                    <div class="box-tools pull-right">
                        <!--                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fa fa-minus"></i></button>
                                                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                        <i class="fa fa-times"></i></button>-->
                    </div>
                </div>
                <div class="box-body">
                    <div class="column col-sm-12">

                        <h1><?= ($model->nome) ? 'Nome '.ucwords($model->nome) : "Atualize" ?></h1>

                        <div class="col-sm-6 b-r">
                            <?php 
                            // View file rendering the widget
                            echo DetailView::widget([
                                'enableEditMode' => false,
                                'labelColOptions' => ['style' => 'width: 50%'],
                                'model' => $model,
                                'striped' => false,
                                'attributes' => [
                                    [
                                            'group'=>true,
                                            'label'=>'Dados de Identificação',
                                            'rowOptions'=>['class'=>'info'],
                                            'groupOptions'=>['class'=>'text-center']
                                    ],
                                    'nome',
                                    'username',
                                    'email',
                                    'cpf',
                                    'celular'
                                ],
                            ]);

                            ?>
                        </div>
                        <div class="col-sm-6 b-r">
                            <?php 
                            // View file rendering the widget
                            echo DetailView::widget([
                                'enableEditMode' => false,
                                'labelColOptions' => ['style' => 'width: 50%'],
                                'model' => $model,
                                'striped' => false,
                                'attributes' => [
                                    [
                                            'group'=>true,
                                            'label'=>'Dados Comerciais',
                                            'rowOptions'=>['class'=>'info'],
                                            'groupOptions'=>['class'=>'text-center']
                                    ],
                                    [
                                            'attribute' => 'carteira_id',
                                            'format' => 'raw',
                                            'label' => 'Carteira',
                                            'value' => ($model->carteira)?$model->carteira->nome:$model->carteira_id,
                                            'type' => KartikDetailView::INPUT_SELECT2,
//                                            
                                    ],
                                    [
                                            'attribute' => 'carteira_id',
                                            'format' => 'raw',
                                            'label' => 'Carteira Limite',
                                            'value' => ($model->carteira)?$leads.'/'.$model->carteira->limit:$model->carteira_id,
                                            'type' => KartikDetailView::INPUT_SELECT2,
//                                            
                                    ],
                                    
                                ],
                            ]);

                            ?>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </section>
</div>
