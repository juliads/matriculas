<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">
  <div class="login-logo">
    <?= Html::a('<b>Gerencie seu</b> Negócio',[''],['style'=>'font-size: 24px;']) ?>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Informe o E-mail para recuperar a conta</p>

    <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'class'=>'form-control'
            ]); ?>
            <div class="form-group has-feedback">
                <?= $form->field($model, 'email')->label(false)->textInput(['autofocus' => true]) ?>
                <span style="font-size: 30px;width: 70px;" class="ion ion-email form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <?= Html::submitButton('Recuperar', ['class' => 'btn btn-primary btn-block margin-top-10', 'name' => 'login-button']) ?>
                </div>
                <!-- /.col -->
            </div>
        <?php ActiveForm::end(); ?>
<!--    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
      <a href="#" class="btn btn-social-icon btn-circle btn-google"><i class="fa fa-google-plus"></i></a>
    </div>-->
    <!-- /.social-auth-links -->

    <div class="margin-top-30 text-center">
        <p>Já possui uma conta? <?= Html::a('Entrar',['default/index'],['class'=>'text-info m-l-5']) ?></p>
    	<p>Não tem uma conta? <?= Html::a('Inscrever-se',['user/register'],['class'=>'text-info m-l-5']) ?></p>
    </div>
    </div>
  <!-- /.login-box-body -->
</div>