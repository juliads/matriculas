<?php

use yii\helpers\Html;
use app\models\Helper;
use kartik\widgets\DynaGrid;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */

$this->title = 'Usuário Lista';
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Usuário
            <small>Lista</small>
        </h1>
        <ol class="breadcrumb">
            <!--<li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Início</a></li>-->
        </ol>
    </section>

    <section class="content">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <div class="form-group">

                    </div>
                    <div class="box-tools pull-right">
                        <!--                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fa fa-minus"></i></button>
                                                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                        <i class="fa fa-times"></i></button>-->
                    </div>
                </div>
                <div class="box-body">
                    <div class="column col-sm-12">
                        <?php
                            // View file rendering the widget
                            //http://demos.krajee.com/grid-demo
                            echo GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'pjax' => true, // pjax is set to always true for this demo
                                'bordered' => true,
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                                'hover' => true,
                                'persistResize' => true,
                                'columns' => [
                                    'username',
                                    'email',
                                    'id',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'header'=>'Ações',
                                        'headerOptions' => ['class'=>'cor-azul','width' => '80'],
                                        'template' => '{editar}{delete}',
                                        'buttons' => [
                                            'editar' => function ($url,$model) 
                                            {
                                                return Html::a('<i class="glyphicon glyphicon-pencil" title="Editar" data-toggle="tooltip"></i>', ['update','id'=>$model->id]).' ';
                                            },
                                            'delete' => function($url,$model)
                                            {
                                                $ativo = ($model->status)?["glyphicon-trash","Deletar"]:["glyphicon-repeat","Ativar"];
                                                return Html::a('<i class="glyphicon '.$ativo[0].'" title="'.$ativo[1].'" data-toggle="tooltip"></i>', ["delete",'id'=>$model->id]).' ';
                                            }, 
                                          ],
                                    ]  ],
                            ]);
                        ?>

                    </div>
                </div>
            </div>
        </section>
    </section>
</div>

    

