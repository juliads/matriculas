<?php

use yii\helpers\Html;
use app\assets\LoginAsset;
use kartik\widgets\Alert;

LoginAsset::register($this);
$this->title = "Matriculas PUC";
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>


        <?php $this->head() ?>

    </head>
    <body class="fixed-left login-page">
        <?php $this->beginBody() ?>
        <?= $content ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
