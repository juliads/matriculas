<?php

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;
use app\models\Helper;
use yii\helpers\Url;

$this->title = "Matriculas PUC";

AppAsset::register($this);

if(!Yii::$app->user->identity)
{
    return Yii::$app->response->redirect(Url::to(['site/index']));
}

$model = User::find()->where(['id' => Yii::$app->user->identity->id])->one();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="description" content="">
        <meta name="keywords" content="coco bootstrap template, coco admin, bootstrap,admin template, bootstrap admin,">
        <meta name="author" content="Huban Creative">

        <?php $this->registerCsrfMetaTags() ?>
        <?php $this->head() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="shortcut icon" href="img/fundopreto.png">
        <link rel="apple-touch-icon" href="../img/fundopreto.png" />
        <link rel="apple-touch-icon" sizes="57x57" href="../img/fundopreto.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="../img/fundopreto.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="../img/fundopreto.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="../img/fundopreto.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="../img/fundopreto.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="../img/fundopreto.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="../img/fundopreto.png" />
    </head>
    <body class="fixed-left">
        <?php $this->beginBody() ?>
        <div id="wrapper" class="enlarged">
            <?= Yii::$app->controller->renderPartial('/site/_menu_top', ['model' => $model]) ?>
            <?= Yii::$app->controller->renderPartial('/site/_menu_left', ['model' => $model]) ?>
            <div class="content-page">
                <div class="content">
                    <?php if (Yii::$app->session->hasFlash('success')): ?>
                        <div class="alert alert-success"><?= Yii::$app->session->getFlash('success'); ?></div>
                    <?php elseif (Yii::$app->session->hasFlash('error')): ?>
                        <div class="alert alert-danger"><?= Yii::$app->session->getFlash('error'); ?></div>
                    <?php elseif (Yii::$app->session->hasFlash('info')): ?>
                        <div class="alert alert-info"><?= Yii::$app->session->getFlash('info'); ?></div>
                    <?php endif; ?>
                    <?= $content ?>
                    <?= Yii::$app->controller->renderPartial('/site/_footer') ?>
                </div>
            </div>
        </div>



        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
<script>
    var resizefunc = [];
</script>