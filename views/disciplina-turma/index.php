<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\DisciplinaTurmaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disciplina Turmas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disciplina-turma-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Disciplina Turma', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'turma_id',
            'disciplina_id',
            'turno',
            'horario',
            //'user_create',
            //'data_create',
            //'user_update',
            //'data_update',
            //'vagas',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
