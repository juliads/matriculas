<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
            'id' => 'login',
        ]);
?>

        <div class="login-wrap animated flipInX">
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success"><?= Yii::$app->session->getFlash('success'); ?></div>
            <?php elseif (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-danger"><?= Yii::$app->session->getFlash('error'); ?></div>
            <?php elseif (Yii::$app->session->hasFlash('info')): ?>
                <div class="alert alert-info"><?= Yii::$app->session->getFlash('info'); ?></div>
            <?php endif; ?>

            <div class="login-block">
                <img src="http://www.pucrs.br/wp-content/themes/pucrs/images/logo_topo.png"
                <form role="form" id='loginform' action="index.html">
                    
                  
                <label>Portal de Matriculas PUCRS </label> 
                  
                    <div class="form-group login-input">
                        <i class="fa fa-user overlay"></i>
                        <?= $form->field($model, 'username')->textInput(['class' => 'form-control text-input', 'placeholder' => 'login', 'required' => 'required'])->label(false); ?>
                    </div>
                    <div class="form-group login-input">
                        <i class="fa fa-key overlay"></i>
                        <?= $form->field($model, 'passwordHash')->passwordInput(['class' => 'form-control text-input', 'placeholder' => '***********', 'required' => 'required'])->label(false); ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?= Html::submitButton('Entrar', ['class' => 'btn btn-success btn-block']) ?>
                            <?php ActiveForm::end() ?>
                             
                        </div>

                    </div>
                </form>
            </div>
        </div>

    </div>
</div>


<div class="md-overlay"></div>

<script>
    var resizefunc = [];
</script>

<?php ?>