<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
            'id' => 'login',
        ]);
?>

<div class="md-modal md-3d-flip-vertical" id="task-progress">
    <div class="md-content">
        <h3><strong>Task Progress</strong> Information</h3>
        <div>
            <p>CLEANING BUGS</p>
            <div class="progress progress-xs for-modal">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                    <span class="sr-only">80&#37; Complete</span>
                </div>
            </div>
            <p>POSTING SOME STUFF</p>
            <div class="progress progress-xs for-modal">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                    <span class="sr-only">65&#37; Complete</span>
                </div>
            </div>
            <p>BACKUP DATA FROM SERVER</p>
            <div class="progress progress-xs for-modal">
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                    <span class="sr-only">95&#37; Complete</span>
                </div>
            </div>
            <p>RE-DESIGNING WEB APPLICATION</p>
            <div class="progress progress-xs for-modal">
                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                    <span class="sr-only">100&#37; Complete</span>
                </div>
            </div>
            <p class="text-center">
                <button class="btn btn-danger btn-sm md-close">Close</button>
            </p>
        </div>
    </div>
</div>

<!-- Modal Logout -->
<div class="md-modal md-just-me" id="logout-modal">
    <div class="md-content">
        <h3><strong>Logout</strong> Confirmation</h3>
        <div>
            <p class="text-center">Are you sure want to logout from this awesome system?</p>
            <p class="text-center">
                <button class="btn btn-danger md-close">Nope!</button>
                <a href="login.html" class="btn btn-success md-close">Yeah, I'm sure</a>
            </p>
        </div>
    </div>
</div>        <!-- Modal End -->		
<!-- Begin page -->
<div class="container">
    <div class="full-content-center">
        <p class="text-center"><a href="#"><img src="../img/login-logo.png" alt="Logo"></a></p>
        <div class="login-wrap animated flipInX">
            <div class="login-block">
                <img src="../images/users/default-user.png" class="img-circle not-logged-avatar">
                <form role="form" id='loginform' action="index.html">
                    <div class="form-group login-input">
                        <i class="fa fa-user overlay"></i>
                        <?= $form->field($model, 'username')->textInput(['class' => 'form-control text-input', 'placeholder' => 'username', 'required' => 'required'])->label(false); ?>
                    </div>
                    <div class="form-group login-input">
                        <i class="fa fa-key overlay"></i>
                        <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control text-input', 'placeholder' => '***********', 'required' => 'required'])->label(false); ?>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <?= Html::submitButton('Login', ['class' => 'btn btn-success btn-block']) ?>
                            <?php ActiveForm::end() ?>
                        </div>
                        <div class="col-sm-6">
                            <a href="register.html" class="btn btn-default btn-block disabled">Register</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<!-- the overlay modal element -->
<div class="md-overlay"></div>
<!-- End of eoverlay modal -->
<script>
    var resizefunc = [];
</script>

<?php if ($nao_logou): ?>
    <script>
        setTimeout(function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "positionClass": "toast-top-center",
                "onclick": function () {
                    location.href = 'index.php?r=caf/fin_pagar_abertas'
                },
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "15000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.error('Nome de usuário ou senha inválidos!', 'ATENÇÃO');

        }, 100);
    </script>
<?php endif; ?>