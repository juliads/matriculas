<?php

use yii\bootstrap\Html;
?>
<div class="left side-menu" <div class="left side-menu" >
>
    <div class="sidebar-inner slimscrollleft">
        <form role="search" class="navbar-form">

        </form>
        <div class="clearfix"></div>
        <div class="profile-info">
            <div class="col-xs-12 mx-auto">
                <div class="profile-text text-center">Bem vindo <b><?= $model->username ?></b></div>
                <div class="profile-buttons">                    
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="clearfix"></div>

        <div id="sidebar-menu">
            <ul>
                <li>
                    <?= Html::a("<i class='icon-home-3'></i><span>Início</span>", ['site/index']) ?>
                </li>
                  <?php if($model->id > 10):?>
                <li>
                    <?= Html::a("<i class='fas fa-list-ol'></i><span>Realizar Matricula</span>", ['disciplina/index']) ?>
                </li>
                <li style="background-color:#68C39F;">
                    <?= Html::a("<i class='far fa-list-alt'></i><span>Comprovante de Matricula</span>", ['disciplina-aluno-turma/comprovante_matricula','id'=>$model->id],['target'=>'_blank']) ?>
                </li>
                 <?php endif; ?>
                <?php if($model->id < 10):?>
                <li class='has_sub'>
                    <?= Html::a("<i class='fa fa-bar-chart'></i><span>Relatórios</span>", ['relatorios/index']) ?>
                    <ul>
                    <li>
                        <?= Html::a('Percentual de matriculas',['disciplina/relatorio_ocup'],['class'=>'btn btn-md btn-success'],['target'=>'_blank']); ?>
                    </li> 
                    <li>
                        <?= Html::a('Alunos em disciplinas',['disciplina/relatorio_alunodisci'],['class'=>'btn btn-md btn-success'],['target'=>'_blank']);?>
                    </li>
                    <li>
                        <?= Html::a('Alunos com créditos',['disciplina/relatorio_alunocred'],['class'=>'btn btn-md btn-success'],['target'=>'_blank']);?>
                    </li>
                    </ul>
                </li>
                <?php endif; ?>
                <li>
                    <?= Html::a('<i class="fa fa-power-off text-red-1"></i> Sair', ['site/logout'], ['title' => "Deslogar"]) ?>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div><br><br><br>
    </div>
</div>