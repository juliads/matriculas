<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'triber',
    'basePath' => dirname(__DIR__),
    'language' => 'pt-br',
    'timeZone' => 'America/Sao_Paulo',
    'bootstrap' => ['log'],
    'modules'   => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'mgPlp0oqA38KbVJ_sG4sk2jK3oo9uiKi',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,//If you don't have authKey column in your DB, set enableAutoLogin field to false
            'enableSession' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'formatter' => [
            'dateFormat' => 'php:d/m/Y', //'short',
            'datetimeFormat' => 'php:d/m/Y H:i',//'short', dd/n/yy H:m
            'decimalSeparator' => ',',
            'thousandSeparator' => '.',
            'defaultTimeZone' => 'America/Sao_Paulo',
            'currencyCode' => 'R$',
       ], 
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {

    require_once(__DIR__ . '/debugs.php');
    // configuration adjustments for 'dev' environment


    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
