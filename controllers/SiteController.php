<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Lead;
use app\models\LeadStatus;
use app\models\Helper;
use app\models\PesquisaPergunta;
use app\models\LoginForm;

class SiteController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [''],
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {

        if (!Yii::$app->user->identity) {
            return $this->redirect(['site/login']);
        }
        
        
        return $this->render('index',[
                
        ]);
    }

    public function actionLogin() {
        $session = \Yii::$app->session;
        $session->open();
        $this->layout = '@app/views/layouts/login';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->passwordHash = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    public function actionLogout() {
        $session = \Yii::$app->session;
        $session->destroy();
        $this->layout = '@app/views/layouts/login';

        return $this->redirect(['site/index']);
    }

}
