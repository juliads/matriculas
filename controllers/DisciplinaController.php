<?php

namespace app\controllers;

use Yii;
use app\models\Disciplina;
use app\models\DisciplinaSearch;
use app\models\DisciplinaAlunoTurmaSearch;
use app\models\DisciplinaAlunoTurma;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class DisciplinaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new DisciplinaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionRelatorio_ocup()
    {
        $disciplinas = DisciplinaAlunoTurma::find()->where(['situacao'=>'MAT'])->all();
        $arr_disci = [];
        foreach($disciplinas as $value)
        {
            if(!array_key_exists($value->id_disciplina, $arr_disci)){
                $arr_disci[$value->id_disciplina] = 1;
            }
            else{
                $arr_disci[$value->id_disciplina] += 1;
            }
        }

        return $this->render('relatorio_ocup',['arr_disci'=>$arr_disci]);
    }
    public function actionRelatorio_alunodisci()
    {
        $searchModel = new DisciplinaAlunoTurmaSearch();
        $dataProvider = $searchModel->search_mat(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        return $this->render('relatorio_alunodisci', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionRelatorio_alunocred()
    {
        $disciplinas = DisciplinaAlunoTurma::find()->where(['situacao'=>'MAT'])->all();
        $arr_aluno = [];
        $total = 0;
        foreach($disciplinas as $value)
        {
            if(!array_key_exists($value->id_user, $arr_aluno)){
                $arr_aluno[$value->id_user] = $value->disciplina->credito;
            }
            else{
                $arr_aluno[$value->id_user] += $value->disciplina->credito;
            }
            $total += $value->disciplina->credito;
        }
        
        return $this->render('relatorio_alunocred',['arr_aluno'=>$arr_aluno,'total'=>$total]);
        
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Disciplina();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

   
    protected function findModel($id)
    {
        if (($model = Disciplina::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
