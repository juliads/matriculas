<?php

namespace app\controllers;
use Yii;
use app\models\DisciplinaAlunoTurma;
use app\models\DisciplinaPreRequisito;
use app\models\DisciplinaAlunoTurmaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class DisciplinaAlunoTurmaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new DisciplinaAlunoTurmaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new DisciplinaAlunoTurma();
        $user = \Yii::$app->user->identity;
        
        if (Yii::$app->request->get()) 
        {
            $get = Yii::$app->request->get();
            $matricula_excluida = DisciplinaAlunoTurma::find()->where(['id_user'=>$user->id,'id_disciplina'=>$get['id_disciplina'],'id_turma'=>$get['id_turma'],'situacao'=>'DEL'])->orderby(['id'=>SORT_DESC])->one();
            
            $pre_requisitos = DisciplinaPreRequisito::find()->where(['disciplina_id'=>$get['id_disciplina']])->all();
            
            foreach($pre_requisitos as $pre_requisito)
            {
                
                $aprovado = DisciplinaAlunoTurma::find()->where([
                    'id_user'=>$user->id,
                    'id_disciplina'=>$pre_requisito->pre_disciplina_id,
                    'situacao'=>'APR'
                    
                ])->orderby(['id'=>SORT_DESC])->all();
                
                if(!$aprovado)
                {
                    Yii::$app->session->setFlash
                            ('error', 'Você não pode se matricular nesta disciplina! Essa disciplina possui pré-requisito.');
                    return $this->redirect(['disciplina/index']);
                }
                
            }
            $count = count(DisciplinaAlunoTurma::find()->where(['id_disciplina'=>$get['id_disciplina'],'id_turma'=>'168','situacao'=>'MAT'])->orderby(['id'=>SORT_DESC])->all());
            if($count<3)
            {
                if($matricula_excluida)
                {
                    $matricula_excluida->situacao = 'MAT';
                    if(!$matricula_excluida->save())
                    {
                        ddd($matricula_excluida->getErrors());
                    }
                
                    Yii::$app->session->setFlash('success', 'Matricula realizada com sucesso.');
                    return $this->redirect(['disciplina/index']);
                }
                $model->id_disciplina = $get['id_disciplina'];
                $model->id_turma = $get['id_turma'];
                $model->situacao = $get['situacao'];
                $model->id_user = $user->id;
                
                if(!$model->save())
                {
                    ddd($model->getErrors());
                }
                
                Yii::$app->session->setFlash('success', 'Matriculado com sucesso.');
            }
            else{
                Yii::$app->session->setFlash('error', 'Limite de vagas atingido.');
            }
            return $this->redirect(['disciplina/index']);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->situacao = 'DEL';

        if (!$model->save()) {
           ddd($model->getErrors()); 
        }
        Yii::$app->session->setFlash('error', 'Matricula excluida.');
        return $this->redirect(['disciplina/index']);
    }
    
    public function actionComprovante_matricula($id)
    {
        $models = DisciplinaAlunoTurma::find()->where(['id_user'=>$id,'situacao'=>'MAT'])->all();
        $this->layout = '@app/views/layouts/relatorio.php';
        
        return $this->render('comprovante_matricula',[
                'models'=>$models
            ]);
    }
    
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = DisciplinaAlunoTurma::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
