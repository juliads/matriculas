-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: car.c8uikejwat5b.sa-east-1.rds.amazonaws.com    Database: m2b
-- ------------------------------------------------------
-- Server version	5.6.29-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `carteira`
--

DROP TABLE IF EXISTS `carteira`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carteira` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `limit` int(5) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carteira`
--

LOCK TABLES `carteira` WRITE;
/*!40000 ALTER TABLE `carteira` DISABLE KEYS */;
INSERT INTO `carteira` VALUES (1,'Teste',50,1),(2,'Sirlei',50,1),(3,'Emmanoel',100,1);
/*!40000 ALTER TABLE `carteira` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compromisso`
--

DROP TABLE IF EXISTS `compromisso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compromisso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `oportunidade_id` int(11) DEFAULT NULL,
  `compromisso_grupo_id` int(11) DEFAULT NULL,
  `compromisso_status_id` int(11) DEFAULT NULL,
  `compromisso_tipo_id` int(11) DEFAULT NULL,
  `lead_id` int(11) DEFAULT NULL,
  `com_quem` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `cep` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `logradouro` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `complemento` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `bairro` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `localidade` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `uf` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `user_cadastro` int(11) DEFAULT NULL,
  `data_atualizado` datetime DEFAULT NULL,
  `user_atualizado` int(11) DEFAULT NULL,
  `inicio` datetime DEFAULT NULL,
  `fim` datetime DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ibfk_compromisso_oportunidade_idx` (`oportunidade_id`),
  KEY `ibfk_compromisso_status_idx` (`compromisso_status_id`),
  KEY `ibfk_compromisso_tipo_idx` (`compromisso_tipo_id`),
  KEY `ibfk_compromisso_user_idx` (`user_id`),
  KEY `ibfk_compromisso_lead_idx` (`lead_id`),
  KEY `ibfk_compromisso_grupo_idx` (`compromisso_grupo_id`),
  CONSTRAINT `ibfk_compromisso_grupos` FOREIGN KEY (`compromisso_grupo_id`) REFERENCES `compromisso_grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ibfk_compromisso_lead` FOREIGN KEY (`lead_id`) REFERENCES `lead` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ibfk_compromisso_oportunidade` FOREIGN KEY (`oportunidade_id`) REFERENCES `oportunidade` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ibfk_compromisso_status` FOREIGN KEY (`compromisso_status_id`) REFERENCES `compromisso_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ibfk_compromisso_tipo` FOREIGN KEY (`compromisso_tipo_id`) REFERENCES `compromisso_tipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ibfk_compromisso_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compromisso`
--

LOCK TABLES `compromisso` WRITE;
/*!40000 ALTER TABLE `compromisso` DISABLE KEYS */;
INSERT INTO `compromisso` VALUES (1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-15 07:00:00','2018-08-15 10:30:00',NULL),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-15 11:00:00','2018-08-15 12:30:00',NULL),(3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-22 08:30:00','2018-08-22 11:00:00',NULL),(4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-30 00:00:00','2018-08-31 00:00:00',NULL);
/*!40000 ALTER TABLE `compromisso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compromisso_grupo`
--

DROP TABLE IF EXISTS `compromisso_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compromisso_grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compromisso_grupo`
--

LOCK TABLES `compromisso_grupo` WRITE;
/*!40000 ALTER TABLE `compromisso_grupo` DISABLE KEYS */;
INSERT INTO `compromisso_grupo` VALUES (1,'Visita',1),(2,'Conferência',1),(3,'Pessoal',1),(4,'Reunião',1),(5,'Ourtos',1),(6,'Consultoria',1),(7,'Contato GP',1);
/*!40000 ALTER TABLE `compromisso_grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compromisso_status`
--

DROP TABLE IF EXISTS `compromisso_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compromisso_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compromisso_status`
--

LOCK TABLES `compromisso_status` WRITE;
/*!40000 ALTER TABLE `compromisso_status` DISABLE KEYS */;
INSERT INTO `compromisso_status` VALUES (1,'Novo'),(2,'Realizado'),(3,'Não Realizado'),(4,'Cancelado');
/*!40000 ALTER TABLE `compromisso_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compromisso_tipo`
--

DROP TABLE IF EXISTS `compromisso_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compromisso_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `compromisso_grupo_id` int(11) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ibfk_compromisso_grupo_idx` (`compromisso_grupo_id`),
  CONSTRAINT `ibfk_compromisso_grupo` FOREIGN KEY (`compromisso_grupo_id`) REFERENCES `compromisso_grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compromisso_tipo`
--

LOCK TABLES `compromisso_tipo` WRITE;
/*!40000 ALTER TABLE `compromisso_tipo` DISABLE KEYS */;
INSERT INTO `compromisso_tipo` VALUES (1,'V1',1,1),(2,'V1+',1,1),(3,'V2',1,1),(4,'V2+',1,1),(5,'V3',1,1),(6,'V3+',1,1),(7,'CC1',2,1),(8,'CC2',2,1),(9,'CC3',2,1),(10,'Comp. Pessoal',3,1),(11,'Reunião de Abertura',4,1),(12,'Reunião Técnica',4,1),(13,'Acompanhamento de Reunião',4,1),(14,'Reunião deNegociação',4,1),(15,'Reunião de Encerramento',4,1),(16,'Reunião de Entrega',4,1),(17,'Capactação de Documentos',4,1),(18,'Reunião de Validação',4,1),(19,'Entrega Técnica',4,1),(20,'Entrega Final',4,1),(21,'Pré-Agendada',5,1),(22,'Plano de Ação',5,1),(23,'Consultoria Implatação',6,1),(24,'Contato Gestão de Projetos - Email',7,1),(25,'Contato Gestão de Projetos - Telefone',7,1),(26,'Contato Gestão de Projetos - Skype',7,1),(27,'REUNIÃO DE REFORÇO NA AGENDA',5,1),(28,'PRÉ AGENDA DE REUNIÃO COM O CLIENTE',5,1);
/*!40000 ALTER TABLE `compromisso_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lead`
--

DROP TABLE IF EXISTS `lead`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lead` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carteira_id` int(11) DEFAULT NULL,
  `lead_status_id` int(11) DEFAULT NULL,
  `lead_situacao_id` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `cep` varchar(45) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `numero` varchar(100) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `estado` varchar(3) DEFAULT NULL,
  `midia` varchar(100) DEFAULT NULL,
  `usercreate` int(11) DEFAULT NULL,
  `datacreate` datetime DEFAULT NULL,
  `userupdate` int(11) DEFAULT NULL,
  `dataupdate` datetime DEFAULT NULL,
  `userperdido` int(11) DEFAULT NULL,
  `dataperdido` datetime DEFAULT NULL,
  `userliberado` int(11) DEFAULT NULL,
  `dataliberado` datetime DEFAULT NULL,
  `userprospeccao` int(11) DEFAULT NULL,
  `dataprospeccao` datetime DEFAULT NULL,
  `usercc1` int(11) DEFAULT NULL,
  `datacc1` datetime DEFAULT NULL,
  `usercc2` int(11) DEFAULT NULL,
  `datacc2` datetime DEFAULT NULL,
  `useroportunidade` int(11) DEFAULT NULL,
  `dataoportunidade` datetime DEFAULT NULL,
  `userproposta` int(11) DEFAULT NULL,
  `dataproposta` datetime DEFAULT NULL,
  `userminutaenviada` int(11) DEFAULT NULL,
  `dataminutaenviada` datetime DEFAULT NULL,
  `usercontratofechado` int(11) DEFAULT NULL,
  `datacontratofechado` datetime DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `ibfk_lead_carteira_idx` (`carteira_id`),
  KEY `ibfk_lead_status_idx` (`lead_status_id`),
  KEY `ibfk_lead_situacao_idx` (`lead_situacao_id`),
  CONSTRAINT `ibfk_lead_carteira` FOREIGN KEY (`carteira_id`) REFERENCES `carteira` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ibfk_lead_situacao` FOREIGN KEY (`lead_situacao_id`) REFERENCES `lead_situacao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ibfk_lead_status` FOREIGN KEY (`lead_status_id`) REFERENCES `lead_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lead`
--

LOCK TABLES `lead` WRITE;
/*!40000 ALTER TABLE `lead` DISABLE KEYS */;
INSERT INTO `lead` VALUES (1,NULL,6,NULL,'','','','','','','','','','','','',NULL,'2018-07-09 12:12:56',1,'2018-08-28 13:28:22',NULL,NULL,NULL,NULL,1,'2018-07-09 11:28:33',1,'2018-07-09 11:37:39',1,'2018-07-09 14:01:26',1,'2018-07-13 13:55:49',1,'2018-06-29 12:45:31',1,'2018-08-28 09:37:37',1,'2018-08-28 09:04:35',1),(2,1,7,NULL,'Emmanoel da silva soares paim','emmanoelpaim1@gmail.com','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-09 12:12:56',1,'2018-08-28 15:05:38',NULL,NULL,NULL,NULL,1,'2018-07-09 11:28:28',1,'2018-07-09 11:37:41',1,'2018-07-09 17:31:32',1,'2018-07-04 15:15:10',1,'2018-07-13 13:55:21',1,'2018-08-28 09:37:38',1,'2018-08-28 15:05:38',1),(3,1,7,NULL,'Emmanoel da silva soares paim','emmanoelpaim2@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-09 12:12:56',1,'2018-08-28 13:27:53',NULL,NULL,NULL,NULL,1,'2018-07-09 11:24:24',1,'2018-07-09 11:37:40',1,'2018-07-09 14:10:51',1,'2018-07-13 13:55:36',1,'2018-07-13 13:55:50',NULL,NULL,1,'2018-08-28 08:57:04',1),(4,1,7,NULL,'Emmanoel da silva soares paim','emmanoelpaim3@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-09 12:12:56',1,'2018-08-28 13:27:54',NULL,NULL,NULL,NULL,1,'2018-07-09 11:28:31',1,'2018-07-09 11:37:42',1,'2018-08-15 15:40:45',1,'2018-08-15 15:29:37',NULL,NULL,1,'2018-07-02 13:58:19',1,'2018-08-28 09:04:37',1),(5,1,7,NULL,'Emmanoel da silva soares paim','emmanoelpaim4@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-09 12:12:56',1,'2018-08-28 13:27:54',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2018-06-29 13:42:18',1,'2018-07-02 08:53:14',NULL,NULL,1,'2018-07-02 08:53:19',1),(6,1,3,1,'Eliana de Maria Ramos da Silva','emmanoelpaim5@gmail.com','(51) 9696-75856','(51) 9858-24571','90420160','Rio Branco','Rua Coronel Paulino Teixeira','Porto Alegre','345','loja','RS','teste',NULL,'2018-07-09 12:12:56',1,'2018-08-30 15:20:29',NULL,NULL,NULL,NULL,1,'2018-07-09 11:28:27',1,'2018-08-30 14:45:53',1,'2018-08-30 15:20:29',1,'2018-08-30 14:45:49',1,'2018-08-29 10:42:48',1,'2018-08-28 16:45:03',1,'2018-08-28 09:04:33',1),(7,1,7,NULL,'Emmanoel da silva soares paim','emmanoelpaim6@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-09 12:12:56',1,'2018-08-30 15:21:18',NULL,NULL,NULL,NULL,1,'2018-07-09 11:28:29',1,'2018-08-30 14:46:06',1,'2018-08-30 14:45:52',1,'2018-08-30 15:20:43',1,'2018-08-30 15:20:31',NULL,NULL,1,'2018-08-30 15:21:18',1),(8,1,7,NULL,'Emmanoel da silva soares paim','emmanoelpaim7@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-09 12:12:56',1,'2018-08-28 13:27:56',NULL,NULL,NULL,NULL,NULL,NULL,1,'2018-07-02 17:28:08',1,'2018-07-06 09:51:37',1,'2018-07-06 09:51:38',1,'2018-06-29 12:58:50',NULL,NULL,1,'2018-07-06 09:51:39',1),(9,1,7,NULL,'Emmanoel da silva soares paim','emmanoelpaim8@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-09 12:12:56',1,'2018-08-28 13:27:57',NULL,NULL,NULL,NULL,1,'2018-07-09 11:24:23',1,'2018-07-09 11:24:20',1,'2018-07-02 15:40:28',1,'2018-06-29 13:25:51',1,'2018-06-29 13:31:32',NULL,NULL,1,'2018-08-28 09:04:46',1),(11,NULL,7,NULL,'','emmanoelpaim@gmail.com','','','','','','','','','','',NULL,NULL,1,'2018-08-28 09:04:40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2018-08-28 09:04:40',1),(12,1,7,1,'Teste Prospecção','prospeccao@gmail.com','','(51) 9858-24571','90460120','Petrópolis','Rua Perpétua Teles','Porto Alegre','345','loja','RS','teste',NULL,NULL,1,'2018-08-30 15:21:21',NULL,NULL,NULL,NULL,1,'2018-08-28 11:26:30',1,'2018-08-28 13:53:08',1,'2018-08-28 16:48:02',1,'2018-08-28 09:37:41',NULL,NULL,1,'2018-08-29 14:52:16',1,'2018-08-30 15:21:21',1),(13,NULL,7,1,'Eliana de Maria Ramos da Silva','prospeccao1@gmail.com','','','90420160','Rio Branco','Rua Coronel Paulino Teixeira','Porto Alegre','345','loja','RS','',1,'2018-07-09 15:29:58',1,'2018-08-28 12:17:58',NULL,NULL,NULL,NULL,1,'2018-07-09 15:29:58',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2018-08-28 09:04:42',1),(14,3,2,1,'Aldo','aldo@gmail.com','12323123122','2131312312_','90420-160','Rio Branco','Rua Coronel Paulino Teixeira','Porto Alegre','85','301','RS',NULL,4,'2018-09-22 23:32:28',4,'2018-09-24 12:51:47',NULL,NULL,NULL,NULL,NULL,NULL,4,'2018-09-24 11:25:20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(15,3,2,1,'Luana','lu@gmail.com','51232323231','233223232','90420-160','Rio Branco','Rua Coronel Paulino Teixeira','Porto Alegre','51','301','RS',NULL,4,'2018-09-22 23:58:17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `lead` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `lead_mes_atual`
--

DROP TABLE IF EXISTS `lead_mes_atual`;
/*!50001 DROP VIEW IF EXISTS `lead_mes_atual`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `lead_mes_atual` AS SELECT 
 1 AS `create`,
 1 AS `update`,
 1 AS `perdido`,
 1 AS `liberado`,
 1 AS `prospeccao`,
 1 AS `cc1`,
 1 AS `cc2`,
 1 AS `oportunidade`,
 1 AS `proposta`,
 1 AS `minutaenviada`,
 1 AS `contratofechado`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `lead_situacao`
--

DROP TABLE IF EXISTS `lead_situacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lead_situacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lead_situacao`
--

LOCK TABLES `lead_situacao` WRITE;
/*!40000 ALTER TABLE `lead_situacao` DISABLE KEYS */;
INSERT INTO `lead_situacao` VALUES (1,'Muito Frio'),(2,'Frio'),(3,'Morno'),(4,'Quente'),(5,'Muito Quente');
/*!40000 ALTER TABLE `lead_situacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lead_status`
--

DROP TABLE IF EXISTS `lead_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lead_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lead_status`
--

LOCK TABLES `lead_status` WRITE;
/*!40000 ALTER TABLE `lead_status` DISABLE KEYS */;
INSERT INTO `lead_status` VALUES (1,'Prospecção',1),(2,'CC1',1),(3,'CC2',1),(4,'Oportunidade',1),(5,'Proposta',1),(6,'Minuta Enviada',1),(7,'Contrato Fechado',1),(8,'Perdido',1),(9,'Liberado',1);
/*!40000 ALTER TABLE `lead_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `lead_total`
--

DROP TABLE IF EXISTS `lead_total`;
/*!50001 DROP VIEW IF EXISTS `lead_total`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `lead_total` AS SELECT 
 1 AS `create`,
 1 AS `update`,
 1 AS `perdido`,
 1 AS `liberado`,
 1 AS `prospecacao`,
 1 AS `cc1`,
 1 AS `cc2`,
 1 AS `oportunidade`,
 1 AS `proposta`,
 1 AS `minutaenviada`,
 1 AS `contratofechado`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `lead_ultimo_mes`
--

DROP TABLE IF EXISTS `lead_ultimo_mes`;
/*!50001 DROP VIEW IF EXISTS `lead_ultimo_mes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `lead_ultimo_mes` AS SELECT 
 1 AS `create`,
 1 AS `update`,
 1 AS `perdido`,
 1 AS `liberado`,
 1 AS `prospeccao`,
 1 AS `cc1`,
 1 AS `cc2`,
 1 AS `oportunidade`,
 1 AS `proposta`,
 1 AS `minutaenviada`,
 1 AS `contratofechado`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `lead_ultimos_seis_meses`
--

DROP TABLE IF EXISTS `lead_ultimos_seis_meses`;
/*!50001 DROP VIEW IF EXISTS `lead_ultimos_seis_meses`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `lead_ultimos_seis_meses` AS SELECT 
 1 AS `create`,
 1 AS `update`,
 1 AS `perdido`,
 1 AS `liberado`,
 1 AS `prospeccao`,
 1 AS `cc1`,
 1 AS `cc2`,
 1 AS `oportunidade`,
 1 AS `proposta`,
 1 AS `minutaenviada`,
 1 AS `contratofechado`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `lead_ultimos_tres_meses`
--

DROP TABLE IF EXISTS `lead_ultimos_tres_meses`;
/*!50001 DROP VIEW IF EXISTS `lead_ultimos_tres_meses`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `lead_ultimos_tres_meses` AS SELECT 
 1 AS `create`,
 1 AS `update`,
 1 AS `perdido`,
 1 AS `liberado`,
 1 AS `prospeccao`,
 1 AS `cc1`,
 1 AS `cc2`,
 1 AS `oportunidade`,
 1 AS `proposta`,
 1 AS `minutaenviada`,
 1 AS `contratofechado`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `menu_permissao`
--

DROP TABLE IF EXISTS `menu_permissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `index` tinyint(1) DEFAULT '1',
  `login` tinyint(1) DEFAULT '1',
  `logout` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `ibfk_menu_user_idx` (`user_id`),
  CONSTRAINT `ibfk_menu_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_permissao`
--

LOCK TABLES `menu_permissao` WRITE;
/*!40000 ALTER TABLE `menu_permissao` DISABLE KEYS */;
INSERT INTO `menu_permissao` VALUES (1,1,1,1,1);
/*!40000 ALTER TABLE `menu_permissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oportunidade`
--

DROP TABLE IF EXISTS `oportunidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oportunidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `produto_id` int(11) NOT NULL,
  `lead_id` int(11) DEFAULT NULL,
  `oportunidade_status_id` int(11) DEFAULT NULL,
  `oportunidade_tipo_id` int(11) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ibfk_oportunidade_produto_idx` (`produto_id`),
  KEY `ibfk_oportunidade_user_idx` (`user_id`),
  KEY `ibfk_oportunidade_lead_idx` (`lead_id`),
  KEY `ibfk_oportunidade_status_idx` (`oportunidade_status_id`),
  KEY `ibfk_oportunidade_tipo_idx` (`oportunidade_tipo_id`),
  CONSTRAINT `ibfk_oportunidade_lead` FOREIGN KEY (`lead_id`) REFERENCES `lead` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ibfk_oportunidade_produto` FOREIGN KEY (`produto_id`) REFERENCES `produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ibfk_oportunidade_status` FOREIGN KEY (`oportunidade_status_id`) REFERENCES `oportunidade_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ibfk_oportunidade_tipo` FOREIGN KEY (`oportunidade_tipo_id`) REFERENCES `oportunidade_tipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ibfk_oportunidade_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oportunidade`
--

LOCK TABLES `oportunidade` WRITE;
/*!40000 ALTER TABLE `oportunidade` DISABLE KEYS */;
INSERT INTO `oportunidade` VALUES (5,1,1,1,1,1,1);
/*!40000 ALTER TABLE `oportunidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oportunidade_status`
--

DROP TABLE IF EXISTS `oportunidade_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oportunidade_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oportunidade_status`
--

LOCK TABLES `oportunidade_status` WRITE;
/*!40000 ALTER TABLE `oportunidade_status` DISABLE KEYS */;
INSERT INTO `oportunidade_status` VALUES (1,'Fechado');
/*!40000 ALTER TABLE `oportunidade_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oportunidade_tipo`
--

DROP TABLE IF EXISTS `oportunidade_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oportunidade_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oportunidade_tipo`
--

LOCK TABLES `oportunidade_tipo` WRITE;
/*!40000 ALTER TABLE `oportunidade_tipo` DISABLE KEYS */;
INSERT INTO `oportunidade_tipo` VALUES (1,'Bom');
/*!40000 ALTER TABLE `oportunidade_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pesquisa_pergunta`
--

DROP TABLE IF EXISTS `pesquisa_pergunta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pesquisa_pergunta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `ativo` tinyint(1) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `user_create` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ibfk_pergunta_user_idx` (`user_create`),
  CONSTRAINT `ibfk_pergunta_user` FOREIGN KEY (`user_create`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pesquisa_pergunta`
--

LOCK TABLES `pesquisa_pergunta` WRITE;
/*!40000 ALTER TABLE `pesquisa_pergunta` DISABLE KEYS */;
INSERT INTO `pesquisa_pergunta` VALUES (1,'O sistema M2B esta começando e gostaria de saber o que você esta achando do sistema?',1,'2018-09-22 17:54:30',1),(2,'O que você achou da pesquisa?',1,'2018-09-22 17:54:30',1);
/*!40000 ALTER TABLE `pesquisa_pergunta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pesquisa_resposta`
--

DROP TABLE IF EXISTS `pesquisa_resposta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pesquisa_resposta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pesquisa_pergunta` int(11) DEFAULT NULL,
  `resposta` int(11) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  `data_resposta` datetime DEFAULT NULL,
  `user_create` int(11) DEFAULT NULL,
  `comentario` text,
  `descricao_pergunta` text,
  PRIMARY KEY (`id`),
  KEY `id_resposta_pergunta_idx` (`id_pesquisa_pergunta`),
  KEY `id_resposta_pessoa_idx` (`user_create`),
  CONSTRAINT `id_resposta_pergunta` FOREIGN KEY (`id_pesquisa_pergunta`) REFERENCES `pesquisa_pergunta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_resposta_pessoa` FOREIGN KEY (`user_create`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pesquisa_resposta`
--

LOCK TABLES `pesquisa_resposta` WRITE;
/*!40000 ALTER TABLE `pesquisa_resposta` DISABLE KEYS */;
INSERT INTO `pesquisa_resposta` VALUES (1,1,4,1,'2018-09-22 22:45:34',4,'',' O sistema M2B esta começando e gostaria de saber o que você esta achando do sistema?'),(2,2,2,1,'2018-09-22 22:45:34',4,'',' O que você achou da pesquisa?');
/*!40000 ALTER TABLE `pesquisa_resposta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produto`
--

DROP TABLE IF EXISTS `produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto`
--

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
INSERT INTO `produto` VALUES (1,'rola');
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT NULL,
  `carteira_id` int(11) DEFAULT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `cpf` varchar(20) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `cpf_UNIQUE` (`cpf`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `celular_UNIQUE` (`celular`),
  KEY `ibfk_carteira_idx` (`carteira_id`),
  CONSTRAINT `ibfk_carteira` FOREIGN KEY (`carteira_id`) REFERENCES `carteira` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'21232f297a57a5a743894a0e4a801fc3',1,'admin','','admin','','',1),(3,'21232f297a57a5a743894a0e4a801fc3',2,'sirlei',NULL,'sirlei',NULL,NULL,1),(4,'21232f297a57a5a743894a0e4a801fc3',3,'emmanoel',NULL,'emmanoel',NULL,NULL,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `lead_mes_atual`
--

/*!50001 DROP VIEW IF EXISTS `lead_mes_atual`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`raphael`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `lead_mes_atual` AS select (select count(`lead`.`datacreate`) from `lead` where (`lead`.`datacreate` between date_format(now(),'%Y-%m-01 00:00:00') and concat(last_day(now()),' 23:59:59'))) AS `create`,(select count(`lead`.`dataupdate`) from `lead` where (`lead`.`dataupdate` between date_format(now(),'%Y-%m-01 00:00:00') and concat(last_day(now()),' 23:59:59'))) AS `update`,(select count(`lead`.`dataperdido`) from `lead` where (`lead`.`dataperdido` between date_format(now(),'%Y-%m-01 00:00:00') and concat(last_day(now()),' 23:59:59'))) AS `perdido`,(select count(`lead`.`dataliberado`) from `lead` where (`lead`.`dataliberado` between date_format(now(),'%Y-%m-01 00:00:00') and concat(last_day(now()),' 23:59:59'))) AS `liberado`,(select count(`lead`.`dataprospeccao`) from `lead` where (`lead`.`dataprospeccao` between date_format(now(),'%Y-%m-01 00:00:00') and concat(last_day(now()),' 23:59:59'))) AS `prospeccao`,(select count(`lead`.`datacc1`) from `lead` where (`lead`.`datacc1` between date_format(now(),'%Y-%m-01 00:00:00') and concat(last_day(now()),' 23:59:59'))) AS `cc1`,(select count(`lead`.`datacc2`) from `lead` where (`lead`.`datacc2` between date_format(now(),'%Y-%m-01 00:00:00') and concat(last_day(now()),' 23:59:59'))) AS `cc2`,(select count(`lead`.`dataoportunidade`) from `lead` where (`lead`.`dataoportunidade` between date_format(now(),'%Y-%m-01 00:00:00') and concat(last_day(now()),' 23:59:59'))) AS `oportunidade`,(select count(`lead`.`dataproposta`) from `lead` where (`lead`.`dataproposta` between date_format(now(),'%Y-%m-01 00:00:00') and concat(last_day(now()),' 23:59:59'))) AS `proposta`,(select count(`lead`.`dataminutaenviada`) from `lead` where (`lead`.`dataminutaenviada` between date_format(now(),'%Y-%m-01 00:00:00') and concat(last_day(now()),' 23:59:59'))) AS `minutaenviada`,(select count(`lead`.`datacontratofechado`) from `lead` where (`lead`.`datacontratofechado` between date_format(now(),'%Y-%m-01 00:00:00') and concat(last_day(now()),' 23:59:59'))) AS `contratofechado` from `lead` limit 1 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `lead_total`
--

/*!50001 DROP VIEW IF EXISTS `lead_total`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`raphael`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `lead_total` AS select (select count(`lead`.`datacreate`) from `lead`) AS `create`,(select count(`lead`.`dataupdate`) from `lead`) AS `update`,(select count(`lead`.`dataperdido`) from `lead`) AS `perdido`,(select count(`lead`.`dataliberado`) from `lead`) AS `liberado`,(select count(`lead`.`dataprospeccao`) from `lead`) AS `prospecacao`,(select count(`lead`.`datacc1`) from `lead`) AS `cc1`,(select count(`lead`.`datacc2`) from `lead`) AS `cc2`,(select count(`lead`.`dataoportunidade`) from `lead`) AS `oportunidade`,(select count(`lead`.`dataproposta`) from `lead`) AS `proposta`,(select count(`lead`.`dataminutaenviada`) from `lead`) AS `minutaenviada`,(select count(`lead`.`datacontratofechado`) from `lead`) AS `contratofechado` from `lead` limit 1 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `lead_ultimo_mes`
--

/*!50001 DROP VIEW IF EXISTS `lead_ultimo_mes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`raphael`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `lead_ultimo_mes` AS select (select count(`lead`.`datacreate`) from `lead` where (`lead`.`datacreate` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(1) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `create`,(select count(`lead`.`dataupdate`) from `lead` where (`lead`.`dataupdate` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(1) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `update`,(select count(`lead`.`dataperdido`) from `lead` where (`lead`.`dataperdido` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(1) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `perdido`,(select count(`lead`.`dataliberado`) from `lead` where (`lead`.`dataliberado` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(1) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `liberado`,(select count(`lead`.`dataprospeccao`) from `lead` where (`lead`.`dataprospeccao` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(1) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `prospeccao`,(select count(`lead`.`datacc1`) from `lead` where (`lead`.`datacc1` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(1) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `cc1`,(select count(`lead`.`datacc2`) from `lead` where (`lead`.`datacc2` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(1) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `cc2`,(select count(`lead`.`dataoportunidade`) from `lead` where (`lead`.`dataoportunidade` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(1) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `oportunidade`,(select count(`lead`.`dataproposta`) from `lead` where (`lead`.`dataproposta` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(1) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `proposta`,(select count(`lead`.`dataminutaenviada`) from `lead` where (`lead`.`dataminutaenviada` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(1) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `minutaenviada`,(select count(`lead`.`datacontratofechado`) from `lead` where (`lead`.`datacontratofechado` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(1) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `contratofechado` from `lead` limit 1 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `lead_ultimos_seis_meses`
--

/*!50001 DROP VIEW IF EXISTS `lead_ultimos_seis_meses`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`raphael`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `lead_ultimos_seis_meses` AS select (select count(`lead`.`datacreate`) from `lead` where (`lead`.`datacreate` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(6) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `create`,(select count(`lead`.`dataupdate`) from `lead` where (`lead`.`dataupdate` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(6) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `update`,(select count(`lead`.`dataperdido`) from `lead` where (`lead`.`dataperdido` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(6) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `perdido`,(select count(`lead`.`dataliberado`) from `lead` where (`lead`.`dataliberado` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(6) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `liberado`,(select count(`lead`.`dataprospeccao`) from `lead` where (`lead`.`dataprospeccao` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(6) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `prospeccao`,(select count(`lead`.`datacc1`) from `lead` where (`lead`.`datacc1` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(6) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `cc1`,(select count(`lead`.`datacc2`) from `lead` where (`lead`.`datacc2` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(6) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `cc2`,(select count(`lead`.`dataoportunidade`) from `lead` where (`lead`.`dataoportunidade` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(6) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `oportunidade`,(select count(`lead`.`dataproposta`) from `lead` where (`lead`.`dataproposta` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(6) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `proposta`,(select count(`lead`.`dataminutaenviada`) from `lead` where (`lead`.`dataminutaenviada` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(6) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `minutaenviada`,(select count(`lead`.`datacontratofechado`) from `lead` where (`lead`.`datacontratofechado` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(6) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `contratofechado` from `lead` limit 1 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `lead_ultimos_tres_meses`
--

/*!50001 DROP VIEW IF EXISTS `lead_ultimos_tres_meses`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`raphael`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `lead_ultimos_tres_meses` AS select (select count(`lead`.`datacreate`) from `lead` where (`lead`.`datacreate` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(3) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `create`,(select count(`lead`.`dataupdate`) from `lead` where (`lead`.`dataupdate` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(3) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `update`,(select count(`lead`.`dataperdido`) from `lead` where (`lead`.`dataperdido` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(3) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `perdido`,(select count(`lead`.`dataliberado`) from `lead` where (`lead`.`dataliberado` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(3) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `liberado`,(select count(`lead`.`dataprospeccao`) from `lead` where (`lead`.`dataprospeccao` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(3) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `prospeccao`,(select count(`lead`.`datacc1`) from `lead` where (`lead`.`datacc1` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(3) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `cc1`,(select count(`lead`.`datacc2`) from `lead` where (`lead`.`datacc2` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(3) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `cc2`,(select count(`lead`.`dataoportunidade`) from `lead` where (`lead`.`dataoportunidade` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(3) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `oportunidade`,(select count(`lead`.`dataproposta`) from `lead` where (`lead`.`dataproposta` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(3) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `proposta`,(select count(`lead`.`dataminutaenviada`) from `lead` where (`lead`.`dataminutaenviada` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(3) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `minutaenviada`,(select count(`lead`.`datacontratofechado`) from `lead` where (`lead`.`datacontratofechado` between (date_format(now(),'%Y-%m-01 00:00:00') + interval -(3) month) and (concat(last_day(now()),' 23:59:59') + interval -(1) month))) AS `contratofechado` from `lead` limit 1 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-24 13:30:08
